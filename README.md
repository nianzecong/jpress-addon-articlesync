<h1 align="center"><a href="http://gitee.com/nianzecong/jpress-addon-articlesync" target="_blank"> jpress-addon-articlesync </a></h1>

<p align="center">
    <a href="http://www.jpress.io" target="_blank">jpress</a>博客插件，可以同步博客到其他平台
</p>


### 功能

- 支持平台：`简书`、`头条号`、`CSDN博客`
- 配置账号数据使用jpress配置的cookie密钥加密入库，防止被拖库导致账号风险
- 可在账号配置界面配置两步验证，防止后台账号被破解导致其他平台账号风险
- 通过谷歌插件一键配置账号
- 后续其他平台正在开发中。

### 版本更新

- V1.0.0 支持简书
- V1.0.1 增加支持头条号，支持平台开关，增加cookie安全提示
- V1.0.2 增加两步验证，数据加密入库
- V1.0.3 支持使用谷歌插件辅助配置账号
- V1.0.4 支持CSDN同步，模块化改进

### 后续计划

- 辅助文档本地化
- 增加博客园支持
- 文章统计抓取和存储

#### 如担心会导致数据风险，请立刻卸载插件，并删除jpress数据库option表中以“articlesync_account_”开头的数据

```SQL
    -- 删除全部插件数据
    DELETE FROM `option` WHERE `key` like "articlesync_%";

    -- 删除账号配置数据
    DELETE FROM `option` WHERE `key` like "articlesync_account_%";

    -- 两步验证重置
    DELETE FROM `option` WHERE `key`='articlesync_ssa_config'
```

### 运行插件

* 插件依赖JPress工程，并作为jpress-addons模块的子模块运行，需要先clone并运行jpress，见：[https://gitee.com/fuhai/jpress](https://gitee.com/fuhai/jpress)。

进入jpress/jpress-addons目录

```shell
cd ~/gitee/jpress/jpress-addons
```

clone 插件工程：

```shell
git clone https://gitee.com/nianzecong/jpress-addon-articlesync
```

使用IDE工具，打开jpress工程，并将插件的pom.xml导入工程

### JUnit调试

首先在`/src/test/resources/`目录创建`jboot.properties`文件，并配置数据库信息，结构与jpress配置文件相同，模板见`jboot-simple.properties`文件。

在`cn.nzcong.jpress.addon.articlesync.test`包下编写JUnit测试文件，并继承`JunitFinalTest`类：

```java
public class MyTest extends JunitFinalTest{

    @Test
    public void optionServiceTest()  {
        // 调用父类中的getService方法获取JFinal中注入的服务
        System.out.println(JSON.toJSONString( super.getService(OptionService.class).findByKey("jpress_version")));

    }

}
```