<h1 align="center">chrome插件本地安装</h1>


### 背景

新版chrome不允许离线安装crx格式的插件，需要使用源码包方式安装。

### 获取源码包

- 使用插件作者提供的源码包，解压
- 将crx格式的插件包扩展名修改为rar，解压

> 插件下载地址：
[`.crx下载`](https://gitee.com/nianzecong/jpress-addon-articlesync/raw/master/src/main/webapp/chrome-ext.crx)、
[`.zip下载`](https://gitee.com/nianzecong/jpress-addon-articlesync/raw/master/src/main/webapp/chrome-ext.zip)

### 源码包安装

访问地址：`chrome://extensions/`

需要开启开发者模式，右上角开关打开

![](./img/chromeext-1.png)

左上角【加载已解压的扩展程序】

![](./img/chromeext-2.png)

选择解压后的源码文件夹，并确定

![](./img/chromeext-3.png)

安装完成！