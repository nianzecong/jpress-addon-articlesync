<h1 align="center">CSDN博客配置说明</h1>

推荐使用[`【谷歌插件】`](./chromeExt.md)一键配置

CSDN需要配置两项设置

- username
- cookie

### username

使用chrome浏览器进入`https://i.csdn.net/`,头像旁边找到`ID:xxxxxxx`,将"ID"的值即为username

![](./img/platCsdn-1.png)

按【F12】进入控制台，选中Network标签，用XHR条件筛选请求，找到随便类型为GET、POST的请求。

在Request Headers中存在cookie值，在cookie中找到`UserToken=xxxxxx;`,复制到cookie设置项中

保存设置，点击【验证】按钮，弹出"验证成功"，说明配置正确

![](./img/platCsdn-2.png)