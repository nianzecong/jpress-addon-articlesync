package cn.nzcong.jpress.addon.articlesync.test;

import cn.nzcong.jpress.addon.articlesync.common.util.HttpUtil;
import cn.nzcong.jpress.addon.articlesync.platform.PlatformCsdn;
import cn.nzcong.jpress.addon.articlesync.platform.PlatformHolder;
import cn.nzcong.jpress.addon.articlesync.vo.config.CsdnConfig;
import com.alibaba.fastjson.JSON;
import io.jpress.service.OptionService;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.stream.Collectors;

public class MyTest extends JunitFinalTest{

    @Test
    public void optionServiceTest()  {
        // 调用父类中的getService方法获取JFinal中注入的服务
        System.out.println(JSON.toJSONString( super.getService(OptionService.class).findByKey("jpress_version")));

    }

    @Test
    public void getAllPlatform(){
        System.out.println(JSON.toJSON(PlatformHolder.getAllPlatform().stream().map(p -> p.getConfigObj()).collect(Collectors.toList())));
    }

    @Test
    public void save(){
        super.getService(PlatformCsdn.class).saveConf(new CsdnConfig(){{
            setCookie("UserToken=0a56d4d220c64e5ebe1f0a46e7b20a2e;");
            setUsername("weixin_40706188");
            setEnable(true);
        }});
    }

    @Test
    public void upload(){
        //http://blog.nzcong.cn/attachment/20191225/a1aaa4ce8cb545a39aa5658a83875d35.jpg
        System.out.println(super.getService(PlatformCsdn.class).upload("http://blog.nzcong.cn/attachment/20191225/a1aaa4ce8cb545a39aa5658a83875d35.jpg"));
    }

    @Test
    public void loadProperties(){
        Properties properties = new Properties();
        try {
            String resp = HttpUtil.httpGet("https://gitee.com/nianzecong/jpress-addon-articlesync/raw/master/src/main/resources/addon.txt");
            for(String line : resp.split("\n")){
                System.out.println(line);
                String[] val = line.split("=");
                properties.setProperty(val[0], val[1]);
            }
            System.out.println(properties.getProperty("version"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}