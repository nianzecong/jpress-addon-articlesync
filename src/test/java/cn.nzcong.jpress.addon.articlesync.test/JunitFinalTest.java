package cn.nzcong.jpress.addon.articlesync.test;

import cn.nzcong.jpress.addon.articlesync.common.util.ClassLoadUtil;
import cn.nzcong.jpress.addon.articlesync.platform.PlatformHolder;
import cn.nzcong.jpress.addon.articlesync.platform.BasePlatform;
import com.jfinal.config.Constants;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.plugin.IPlugin;
import io.jboot.aop.JbootAopFactory;
import org.junit.After;
import org.junit.Before;

import java.util.stream.Collectors;

public class JunitFinalTest {
    private Constants constants;
    private Plugins plugins;

    protected <T> T getService(Class<T> clazz){
        return JbootAopFactory.me().get(clazz);
    }

    /**
     * 通过配置类启动jfinal插件等
     */
    @Before
    public void initConfig() {
        try {
            String configClass = "io.jboot.core.JbootCoreConfig";
            Class<?> clazz = Class.forName(configClass);
            JFinalConfig jfinalConfig = (JFinalConfig) clazz.newInstance();
            constants = new Constants();
            jfinalConfig.configConstant(constants);
            plugins = new Plugins();
            jfinalConfig.configPlugin(plugins);
            for (IPlugin plug : plugins.getPluginList()) {
                plug.start();
            }
            jfinalConfig.afterJFinalStart();
            // junit的类与jpress中的类加载方式不同，junit中需要单独加载各插件的实现bean
            initPlatforms();
            System.out.println("\n==JunitFinalTest Start==================\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 停止jfinal插件
     */
    @After
    public void endConfig() {
        System.out.println("\n==JunitFinalTest End====================");
        if (plugins != null) {
            for (IPlugin plug : plugins.getPluginList()) {
                plug.stop();
            }
        }
//        System.exit(0);
    }

    /**
     * 加载插件实现类
     */
    private void initPlatforms(){
        // 读取classpath中的平台类并初始化
        PlatformHolder.init(ClassLoadUtil.getClassSet(BasePlatform.class.getPackage().getName())
                .stream()
                .filter(clazz -> !clazz.getName().matches(".+\\$.+"))
                .filter(clazz -> clazz.getSuperclass() == BasePlatform.class)
                .map(clazz -> clazz.getName())
                .collect(Collectors.toSet()));
    }

}
