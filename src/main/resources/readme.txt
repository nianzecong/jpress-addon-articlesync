### 安全提示

- cookie泄露有风险，本插件已采用加密等操作尽可能保证账号安全，操作请谨慎，若担心账号风险请立即停止使用本插件
- 随着各平台发展和接口演变本插件所用接口可能已不再适用，请及时联系作者进行更新：<a target='_blank' href='https://gitee.com/nianzecong/jpress-addon-articlesync/stargazers'><img src='https://gitee.com/nianzecong/jpress-addon-articlesync/badge/star.svg?theme=dark' alt='star'></img></a>

### 谷歌插件一键配置账号

- 离线包安装方法：[`点击查看`](https://gitee.com/nianzecong/jpress-addon-articlesync/blob/master/doc/chromeExt.md)

### 两步验证

- 使用谷歌验证器扫描二维码添加账号，不知道是啥？参考文章[《如何使用谷歌身份验证器》](https://www.jianshu.com/p/b6fb07096e0e)
- 10分钟内无验证操作需要再次验证

> ```SQL
-- 两步验证 重置方法
DELETE FROM `option` WHERE `key`='articlesync_ssa_config'
```