/**
 * 定义一个require模块，并依赖app模块
 */
define(["app", "util/current-tab"], function(app, currentTab) {

    // 配置存储在localStorage中的key
    var LOCAL_STORAGE_KEY_ACC = "acc_jpress"
    // 后台地址 - 默认插件配置页地址
    var PLUGIN_URL_PAGE_CONFIG = "/admin/articlesync/configPage";
    // 后台地址 - 查询博客配置地址
    var PLUGIN_URL_GET_CONFIG = "/admin/articlesync/getConfig";
    // 后台地址 - 查询平台配置
    var PLUGIN_URL_GET_PLUGINS = "/admin/articlesync/getPlatforms";
    // 后台地址 - 设置平台账号
    var PLUGIN_URL_DO_CONFIG = "/admin/articlesync/doConfig";

    // 当前生效的配置项
    var config;
    // 默认配置项
    var defaultConfig = {
        protocol : "",
        domain : "",
        cookieDomain : "",
        userName : "",
        nickname : "访问博客后台点击登陆",
        avatar : "./img/jpress.jpg",
        jpuid : false,
        jpssa : false,
        ssaEnable : false
    }

    var getJpressUrl = function(url){
        if(!config.jpuid){
            $.toptip("请先登陆博客", 1000, "warn")
        }
        return config.protocol + "://" + config.domain + (url.indexOf("/") === 0 ? url : "/" + url);
    }

    /**
     * 从localStorage查询配置
     * @return {{
     *      protocol: string,
     *      domain: string,
     *      userName: string,
     *      nickname: string,
     *      avatar: string,
     *      jpuid: boolean
     *  }|*}
     */
    var getConfig = function(){
        config = defaultConfig;
        var LS_config = localStorage[LOCAL_STORAGE_KEY_ACC];
        console.warn("popup-jpress.js [initConfig] local-storage-config = " + LS_config);
        if(LS_config){
            try{
                config = JSON.parse(LS_config)
                // return config;
            } catch (e) {
                console.error("popup-jpress.js [getConfig] - 格式化配置错误");
            }
        }

        // 调用api查询当前页面对应的cookie
        chrome.cookies.getAll({
            "domain" : currentTab.cookieDomain
        }, function(cookies){
            var jpuid = false;
            var jpssa = false;
            for(var i in cookies){
                if(cookies[i].name === "_jpuid"){
                    jpuid = cookies[i].value;
                } else if(cookies[i].name === "j_ssa"){
                    jpssa = cookies[i].value;
                }
            }

            if(config.jpuid && jpuid){
                if(jpuid !== config.jpuid){
                    $.confirm("检测到已登陆其他博客后台，注销重新登陆？", "", logout)
                } else if(jpssa){
                    config.jpssa = jpssa;
                    //TODO 两步验证就位后初始化平台配置信息
                }
            }
        });
        console.warn("popup-jpress.js [initConfig] - config = " + JSON.stringify(config))
        return config;
    }

    /**
     * 初始化配置文件
     */
    var initConfig = function(){
        // 处理端口号，cookie记录域名时忽略端口号
        // 调用api查询当前页面对应的cookie
        chrome.cookies.getAll({
            "domain" : currentTab.cookieDomain
        }, function(cookies){
            var jpuid = false;
            var jpssa = false;
            for(var i in cookies){
                if(cookies[i].name === "_jpuid"){
                    jpuid = cookies[i].value;
                } else if(cookies[i].name === "j_ssa"){
                    jpssa = cookies[i].value;
                }
            }
            // 找到cookie说明从jpress页面打开并且已经登陆
            if(!jpuid){
                $.alert("请登陆博客后台再次启动插件", "");
                return;
            }
            var newConf = config;

            // 拼装cookie
            var cookies = {
                _jpuid: jpuid,
                j_ssa: jpssa
            };
            // 调用插件接口查询登陆用户信息，此处使用当前页面的协议和域名
            sendJpressPost(currentTab.createUrl(PLUGIN_URL_GET_CONFIG), cookies, "")
                .done(function(data){
                    //{"state":"ok","config":{"nickname":"N小聪Dev","avatar":"/attachment/20191228/0778244f901c4fec8c462b138a834705.png","userName":"admin","ssaEnable":false}}
                    if("ok" !== data.state){
                        $.toptip(data.message || "服务错误", 1000, "error")
                    }
                    console.warn("popup-jpress.js [initConfig] - sendPost.response = " + JSON.stringify(data));
                    newConf = {
                        protocol : currentTab.protocol,
                        domain : currentTab.domain,
                        userName : data.config.userName,
                        nickname : data.config.nickname,
                        avatar : data.config.avatar.indexOf("http") === 0 ? data.config.avatar : currentTab.createUrl(data.config.avatar),
                        jpuid : jpuid,
                        jpssa : jpssa,
                        cookieDomain : currentTab.cookieDomain,
                        ssaEnable : data.config.ssaEnable
                    };
                    console.warn("popup-jpress.js  [initConfig.sendPost] - newConfig = " + JSON.stringify(newConf));
                    setConfig(newConf);
                    if(newConf.ssaEnable && !jpssa){
                        $.confirm("两步验证已开启，是否去验证？", "两步验证", function(){
                            goConfigPage();
                        })
                    }
                }).fail(function(e){
                    if(e.errcode === 404){
                        $.alert("<div style='text-align:left'>登陆失败，可能的原因：<br/>- 当前非JPress博客页面<br/>- 插件未安装或不是新版(v1.0.3)</div>", "");
                    } else if(e.errcode === -1){
                        $.alert("请刷新页面后重试", "")
                    } else {
                        $.alert(JSON.stringify(e), "内部错误");
                    }
                });
        });
    }

    /**
     {
         "state":"ok",
         "platforms":[
            {
                "enable":true,
                "valid":true,
                "reason":"",
                "nickname":"N小聪",
                "platformName":"jianshu",
                "userId":"20451298",
                "platformDisplayName":"简书"
            }
         ]
     }
     */
    var getPlatforms = function(){
        return $.ajax({
            url: getJpressUrl(PLUGIN_URL_GET_PLUGINS),
            type:'post',
            dataType:'json',
        }).done(function(resp){
            console.info("popup-jpress.js [getPlatforms] - " + JSON.stringify(resp))
            if(resp.state !== "ok") {
                throw {message: "服务异常"}
            }
        });
    }

    /**
     * 保存平台配置
     * @param config 平台配置
     * @return $.ajax
     */
    var doSavePlatform = function(platformName, config){
        return $.ajax({
            url: getJpressUrl(PLUGIN_URL_DO_CONFIG),
            type:'post',
            dataType:'json',
            data: {
                platform : platformName,
                conf : config
            }
        }).done(function(resp){
            console.info("popup-jpress.js [doSavePlatform] - " - JSON.stringify(resp))
            if(resp.state !== "ok") {
                $.alert(resp.message, "保存失败");
                return;
            }
            $.toptip("设置成功", 500, "success")
            setTimeout(function(){
                window.location.reload(true);
            }, 500)
        }).fail(function(resp){
            if(resp.responseText.indexOf("两步验证") > 0){
                $.confirm("前往验证？", "两步验证已开启", goConfigPage);
            } else {
                $.toptip("系统异常", 1000, "error");
            }
        });
    }


    /**
     * 向jpress后端发送post请求，封装了csrf逻辑
     * @param url
     * @param cookies
     * @param data
     * @return {返回一个promise对象}
     */
    var sendJpressPost = function(url, cookies, data){
        var csrf = new Date().getTime();
        // url 拼接参数
        var urlObj = new URL(url);
        urlObj.searchParams.append("csrf_token", csrf);
        cookies["csrf_token"] = csrf;
        return app.sendPost(currentTab.tabId, urlObj.href, cookies, data);
    }

    /**
     *  账号配置存入localStorage
     */

    var setConfig = function(newConfig){
        var doSet = function(newConfig){
            localStorage[LOCAL_STORAGE_KEY_ACC] = newConfig ? JSON.stringify(newConfig) : undefined;
            config = newConfig
            window.location.reload(true);
        }

        // 存在旧配置，且非同一站点、非同一用户，弹confirm
        if(!newConfig){
            doSet(null);
        } else if(config.jpuid && (config.domain !== newConfig.domain || config.userName !== newConfig.userName)){
            $.confirm({
                title: '确定替换?',
                text:
                    '由<b>' + config.userName + "@" + config.domain +
                    "</b>替换为：<b>" + newConfig.userName + "@" + newConfig.domain + "</b>",
                onOK: function(){
                    doSet(newConfig);
                }
            });
        } else {
            doSet(newConfig);
        }

    }

    /**
     * 当前tab跳转到插件配置界面
     */
    var goConfigPage = function(){
        chrome.tabs.update(currentTab.tabId, {url: getJpressUrl(PLUGIN_URL_PAGE_CONFIG)});
    }

    /**
     * 展示jpress配置项的菜单
     */
    var showMenu = function(){
        $.actions({
            actions: [{
                text: "访问插件页面",
                onClick: goConfigPage
            },{
                text: "注销",
                onClick: logout
            }]
        });
    }

    var logout = function(){
        // 保留原地址，设置后清空
        var domain = config.domain;
        setConfig(undefined);
        $.alert("已从" +  domain + "登出", function() {
            //点击确认后的回调函数
            window.location.reload();
        });
    }


    // 向require外部暴露方法
    return {
        // initPopup : initPopup,   // 出事画popup页面对应菜单
        initConfig : initConfig, // 初始化配置
        getConfig : getConfig,   // 获取配置
        goConfigPage : goConfigPage, // 跳转到配置界面
        showMenu : showMenu,    // 显示操作菜单
        getPlatforms : getPlatforms,// 查询平台配置
        doSavePlatform : doSavePlatform // 保存平台配置
    }

});
