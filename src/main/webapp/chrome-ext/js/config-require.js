
// 获取当前js所在目录
var _js=document.scripts;
window._baseUrl=_js[_js.length-1].src.substring(0,_js[_js.length-1].src.lastIndexOf("/")+1);

// 配置require
require.config({
	baseUrl: _baseUrl,
	// urlArgs: "_=" + _ver ,
    waitSeconds: 60,
    paths : {
        "jquery" 		: "lib/jquery-2.1.4.min",
        "jquery-pjax" 	: "lib/jquery.pjax.min",
        "jquery-cookie" : "lib/jquery.cookie",
        "jqweui"		: ["lib/jquery-weui.min"],
        "SHA512"		: "lib/SHA512",
        "base64"     	: "lib/base64",
        "app"			: "app",
    },
    map: {
        '*': {css: ["lib/require/require-css.min"]}
    },
    shim: {
        "jqweui" : {
            deps: [
                "jquery",
                "css!https://cdn.bootcss.com/weui/1.1.3/style/weui.min.css",
                "css!https://cdn.bootcss.com/jquery-weui/1.2.1/css/jquery-weui.min.css"
            ]
        },
        "jquery-pjax" : {deps: ["jquery"]},
        "app": {deps: ["jqweui"]},
    }
});