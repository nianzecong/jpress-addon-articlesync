var foo = function(){
    console.warn("content-script 123 - " + typeof _sendPost);

    // 收到消息
    chrome.extension.onMessage.addListener(
        function(msg, sender, callbackFunc) {
            //{
            // "func":"sendPost",
            // "data":{
            //  "url":"http://localhost:8080/admin/articlesync/getConfig?csrf_token=1577810174064",
            //  "cookie":{"csrf_token":1577810174064,"_jpuid":"\"ZWFjMzAzNmU4Y2RmYjJiMjQ1M2JiNTFhNmRjMjhmOWQjMTU3NzQ2OTkzMDIzNyM2MDQ4MDAjTVE9PQ==\""},
            //  "data":""}
            // }

            // console.warn("onMessage proxy data - " + JSON.stringify(data));
            var onSuccess = function(result){
                var respData = {
                    code : 0,
                    data : result
                }
                if("function" === typeof callbackFunc){
                    callbackFunc(respData)
                }
            }

            var onError = function(result){
                var respData = {
                    code : result.code || -1,
                    data : result
                }
                if("function" === typeof callbackFunc){
                    callbackFunc(respData)
                }
            }

            var evalStr = msg.func + "(" + (typeof msg.data === "string" ? msg.data : JSON.stringify(msg.data)) + ", onSuccess, onError);"
            console.warn("onMessage proxy evalStr - javascript:" + evalStr);

            var resp = eval(evalStr);
            return true;
        });



    function sendPost(param, successFunc, failFunc){
        var cookieStr = "";
        for(var key in param.cookie){
            cookieStr += (key + ":" + param.cookie[key] + ";");
        }
        var headers = {
            'Content-Type':'application/json;charset=utf8'
        }
        if(cookieStr.length > 0){
            headers["cookie"] = cookieStr;
        }
        ajax({
            url: param.url,
            type:'post',
            dataType:'json',
            async:false,
            headers:headers,
            data: param.data
        })
            .then(successFunc)
            .catch(failFunc);
    }

    function sendGet(param, successFunc, failFunc){
        var cookieStr = "";
        for(var key in param.cookie){
            cookieStr += (key + ":" + param.cookie[key] + ";");
        }
        var headers = {
            'Content-Type':'application/json;charset=utf8'
        }
        if(cookieStr.length > 0){
            headers["cookie"] = param.cookie;
        }
        ajax({
            url: param.url,
            type:'get',
            dataType:'json',
            async:false,
            headers:headers,
            data: param.data
        })
            .then(successFunc)
            .catch(failFunc);
    }


    /**
     *  param = {
     *      url : 'url',
     *      async : true | false,
     *      type : 'get' | 'post',
     *      dataType : null | 'json',
     *      header : {xxx},
     *      data : {...} | '',
     *      success : function,
     *      error : function
     *  }
     */

    function ajax(param){

        //1.创建ajax对象
        if(window.XMLHttpRequest){
            oAjax=new XMLHttpRequest();
        }else{
            oAjax=new ActiveXObject("Microsoft.XMLHTTP");
        }

        type = param.type ? param.type.toUpperCase() : "GET";

        var data = param.data;
        var isPost = "POST" === type
        if(isPost && param.data){
            if("json" === param.dataType){
                data = JSON.stringify(data);
            } else {
                var dataStr = "";
                for(var key in data){
                    dataStr += "&" + key + "=" + data[key]
                }
                data = dataStr;
            }
        }

        return new Promise(function(resolve, reject){ //做一些异步操作
            oAjax.onreadystatechange=function(){
                if (oAjax.readyState === 1){
                    return;
                }
                if (oAjax.readyState === 4 && oAjax.status === 200){
                    if(param.success){
                        param.success(oAjax.response);
                    }
                    resolve(oAjax.response);
                } else {
                    var e = {
                        code:oAjax.status,
                        message:oAjax.response
                    }
                    if(param.error){
                        param.error(e);
                    }
                    reject(e);
                }
            }
            oAjax.open(param.type, param.url, true === param.async);

            if(param.headers){
                for(var key in param.headers){
                    oAjax.setRequestHeader(key, param.headers[key]);  //设置请求头
                }
            }

            oAjax.send(isPost ? data : null);
        });
    }
}();
