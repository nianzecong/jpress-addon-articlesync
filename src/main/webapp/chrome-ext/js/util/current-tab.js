/**
 * 定义一个require模块
 */
define(["app"], function() {

    /**
     * 用当前tab页的协议、域名拼装路径
     * @param uri
     * @return {string}
     */
    var createUrl = function(uri){
        return _currentTab.protocol + "://" + _currentTab.domain + (uri.indexOf("/") === 0 ? uri : "/" + uri);
    }


    var _currentTab = {
        tabId : false,
        // 当前页的协议
        protocol : false,
        // 当前页的域名
        domain : false,
        // cookie的域名，不包含端口号
        cookieDomain : false,
        createUrl : createUrl
    }

    // 异步执行
    chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
        if(!(tabs && tabs[0])){
            return;
        }
        var url = tabs[0].url;
        _currentTab.protocol = url.substr(0, url.indexOf(":"));

        url = tabs[0].url.replace(/https?:\/\//, "");
        _currentTab.domain = url.substr(0, url.indexOf("/"))
        // console.warn("protocol: " + protocol + ", domain: " + domain);
        _currentTab.cookieDomain = _currentTab.domain;
        if(_currentTab.domain.indexOf(":") > 0){
            // 带端口号
            _currentTab.cookieDomain = _currentTab.domain.substr(0, _currentTab.domain.indexOf(":"))
        }
        _currentTab.tabId = tabs[0].id;
    });

    /**
     * 向外暴露方法
     */
    return _currentTab;

})