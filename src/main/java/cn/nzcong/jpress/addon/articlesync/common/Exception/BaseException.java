package cn.nzcong.jpress.addon.articlesync.common.Exception;

/**
 * 基础异常类
 *
 * @author zecong.nian
 */
public abstract class BaseException extends RuntimeException {

    int errcode = 0;

    abstract ErrorCode getErrorCode();

    public BaseException(){
        this.errcode = this.getErrorCode().code;
    }

    public BaseException(Throwable e, String msg, String ... args){
        super(String.format(msg, args), e);
        this.errcode = this.getErrorCode().code;
    }

    public BaseException(String msg, String ... args){
        super(String.format(msg, args));
        this.errcode = this.getErrorCode().code;
    }

    public BaseException(Throwable e, String msg){
        super(msg, e);
        this.errcode = this.getErrorCode().code;
    }

    public BaseException(String msg){
        super(msg);
        this.errcode = this.getErrorCode().code;
    }

}
