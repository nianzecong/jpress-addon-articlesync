package cn.nzcong.jpress.addon.articlesync.platform;

import cn.nzcong.jpress.addon.articlesync.common.Constant;
import cn.nzcong.jpress.addon.articlesync.common.Exception.BaseException;
import cn.nzcong.jpress.addon.articlesync.common.Exception.InternalServiceException;
import cn.nzcong.jpress.addon.articlesync.common.Exception.InvalidParamException;
import io.jboot.aop.JbootAopFactory;
import io.jpress.core.addon.AddonClassLoader;
import io.jpress.core.addon.AddonInfo;
import io.jpress.core.addon.AddonManager;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 平台配置，用来整合平台的数据
 *
 * @author zecong.nian
 */
public class PlatformHolder {

    /**
     * 所有支持平台
     */
    private static volatile List<BasePlatform> allPlatform = null;

    /**
     * 查询所有支持的平台
     *
     * @return
     */
    public static List<BasePlatform> getAllPlatform(){
        return allPlatform;
    }

    /**
     * 用插件信息初始化平台
     */
    public static void init(AddonInfo addonInfo){
        try {
            init(new AddonClassLoader(addonInfo).getClassNameList().stream()
                    .filter(name -> !name.matches(".+\\$.+"))
                    .filter(name -> {
                        try {
                            return Class.forName(name).getSuperclass() == BasePlatform.class;
                        } catch (ClassNotFoundException e) {
                            throw new InternalServiceException(e, "加载平台失败 - 类不存在");
                        }
                    })
                    .collect(Collectors.toSet()));
        } catch (IOException e) {
            throw new InternalServiceException(e, "初始化平台列表失败");
        }
    }

    /**
     * 按class重新初始化平台列表
     */
    public static synchronized void init(Set<String> clazzNames){
        allPlatform = new ArrayList<>();
        for (String name : clazzNames) {
            try {
                System.out.println("加载平台类：" + name);
                allPlatform.add(BasePlatform.class.cast(JbootAopFactory.me().get(Class.forName(name))));
            } catch (ClassNotFoundException e) {
                throw new InternalServiceException(e, "加载平台失败");
            }
        }
    }

    /**
     * 启用了的平台
     * @return
     */
    public static List<BasePlatform> getEnablePlatform(){
        return getAllPlatform().stream()
                .filter(BasePlatform::enable)
                .collect(Collectors.toList());
    }

    /**
     * 根据名字获取平台
     *
     * @param name
     * @return
     */
    public static BasePlatform getByName(String name){
        if(StringUtils.isBlank(name)){
            throw new InvalidParamException("平台名称为空");
        }
        for(BasePlatform basePlatform : getAllPlatform()){
            if(name.equalsIgnoreCase(basePlatform.getPlatformEnum().getName())){
                return basePlatform;
            }
        }

        throw new InvalidParamException("不支持的平台（%s）", name);
    }


}
