package cn.nzcong.jpress.addon.articlesync.common.Exception;

/**
 * 错误码
 *
 * @author zecong.nian
 */
public enum ErrorCode {

    PARAM_ERROR(400, "参数错误"),
    AUTH_ERROR(401, "鉴权错误"),
    TOO_MANY_REQUESTS(429, "请求受限"),
    INTERNAL_ERROR(500, "内部错误"),

    EXTERNAL_ERROR(50001, "外部接口错误"),
    ;
    int code;
    String msg;

    ErrorCode(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

}
