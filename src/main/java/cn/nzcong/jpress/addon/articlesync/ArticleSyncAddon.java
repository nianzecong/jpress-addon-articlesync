package cn.nzcong.jpress.addon.articlesync;

import cn.nzcong.jpress.addon.articlesync.common.Constant;
import cn.nzcong.jpress.addon.articlesync.platform.BasePlatform;
import cn.nzcong.jpress.addon.articlesync.platform.PlatformHolder;
import cn.nzcong.jpress.addon.articlesync.service.impl.ConfigOptionService;
import com.jfinal.aop.Inject;
import io.jpress.core.addon.Addon;
import io.jpress.core.addon.AddonInfo;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;

/**
 * 这是一个 JPress 插件的 hello world 项目，没有具体的功能。
 *
 * 其存在的目的是为了帮助开发者，通过 hello world ，了解如何开发一个 JPress 插件
 *
 */
public class ArticleSyncAddon implements Addon {

    @Inject
    ConfigOptionService configOptionService;

    @Override
    public void onInstall(AddonInfo addonInfo) {

        /**
         * 在 onInstall ，我们一般需要 创建自己的数据表
         *
         * onInstall 方法只会执行一次，执行完毕之后不会再执行，除非是用户卸载插件再次安装
         */
        System.out.println("ArticleSyncAddon onInstall");

    }

    @Override
    public void onUninstall(AddonInfo addonInfo) {

        /**
         *  在 onUninstall 中，我们一般需要去删除自己在 onInstall 中创建的表 或者 其他资源文件
         *  这个方法是用户在 Jpress 后台卸载插件的时候回触发。
         */
        // 删除插件数据
        configOptionService.deleteSecondStepConfig();
        List<BasePlatform> allPlatform = PlatformHolder.getAllPlatform();
        for(BasePlatform platform : allPlatform){
            platform.delConf();
        }

        System.out.println("ArticleSyncAddon onUninstall");
    }

    @Override
    public void onStart(AddonInfo addonInfo) {
        /**
         *  在 onStart 方法中，我们可以做很多事情，例如：创建后台或用户中心的菜单
         *
         *  此方法是每次项目启动，都会执行。
         *
         *  同时用户也可以在后台触发
         */
        System.out.println("ArticleSyncAddon onStart");
        // 初始化平台信息
        PlatformHolder.init(addonInfo);
        Constant.ADDON_ID = addonInfo.getId();
        Constant.ADDON_VER = addonInfo.getVersion();
        Constant.AUTHOR_WEBSITE = addonInfo.getAuthorWebsite();
    }

    @Override
    public void onStop(AddonInfo addonInfo) {

        /**
         *  和 onStart 对应，在 onStart 所处理的事情，在 onStop 应该释放
         *
         *  同时用户也可以在后台触发
         */

        System.out.println("ArticleSyncAddon onStop");
        // 重置缓存
        PlatformHolder.getAllPlatform().forEach(BasePlatform::refreshConfig);

    }


    // 定义接口
    public interface FooDao {
        void doSomething();
    }

    // 定义实现
    static class FooDaoImpl implements FooDao{
        @Override
        public void doSomething() {
            System.out.println("Impl....");
        }
    }

    // 代理处理器
    static class FooInvocationHandler implements InvocationHandler{

        // 代理的对象
        Object target;

        public FooInvocationHandler(Object target) {
            this.target = target;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            System.out.println("before");
            Object result = method.invoke(target, args);// 执行代理服务的方法
            System.out.println("after");
            return result; // 返回的是代理对象
        }
    }


    static class FooProxyCjlib implements MethodInterceptor {

        private Object target;

        // 持有被代理对象
        public FooProxyCjlib(Object target) {
            this.target = target;
        }

        @Override
        public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
            System.out.println("before");
            Object invoke = method.invoke(target, objects);
            System.out.println("after");
            return invoke;
        }
    }


    public static void main(String[] args) {
        String str = "";
        for(int i = 0 ; i < 9000000 ; i++){
            if(i % 10000 == 0){
                System.out.println(i);
            }
            str += i;
        }
        System.out.println(str);
    }
}
