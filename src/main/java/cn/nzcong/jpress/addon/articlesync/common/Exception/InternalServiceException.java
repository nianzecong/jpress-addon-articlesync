package cn.nzcong.jpress.addon.articlesync.common.Exception;

/**
 * 内部错误
 *
 * @author zecong.nian
 */
public class InternalServiceException extends BaseException {

    @Override
    ErrorCode getErrorCode() {
        return ErrorCode.INTERNAL_ERROR;
    }

    public InternalServiceException(){
        super();
    }

    public InternalServiceException(Throwable e, String msg, String ... args){
        super(e, msg, args);
    }

    public InternalServiceException(String msg, String ... args){
        super(msg, args);
    }

    public InternalServiceException(Throwable e, String msg){
        super(e, msg);
    }

    public InternalServiceException(String msg){
        super(msg);
    }

}
