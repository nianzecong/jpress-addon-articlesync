package cn.nzcong.jpress.addon.articlesync.vo.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SecondStepConfig extends BaseConfig{

    // 两步验证是否开启
    String key;


}
