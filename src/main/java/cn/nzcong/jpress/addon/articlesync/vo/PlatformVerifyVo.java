package cn.nzcong.jpress.addon.articlesync.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 平台验证结果，有效时返回平台名称
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlatformVerifyVo {

    // 是否有效
    boolean isValid = false;
    // 是否开启
    boolean enable = false;
    
    // 无效原因
    String reason;

    // 平台名称
    String platformName;
    // 平台显示名称
    String platformDisplayName;
    // 账号id
    String userId;
    // 昵称
    String nickname;

}
