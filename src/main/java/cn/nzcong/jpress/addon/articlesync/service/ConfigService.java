package cn.nzcong.jpress.addon.articlesync.service;

import cn.nzcong.jpress.addon.articlesync.vo.config.BaseConfig;
import cn.nzcong.jpress.addon.articlesync.vo.config.SecondStepConfig;

public interface ConfigService {


    /**
     * 保存插件配置
     * @param jsonObject
     */
    void saveSecondStepConfig(SecondStepConfig jsonObject);

    /**
     * 查询插件配置
     *
     * @return
     */
    SecondStepConfig getSecondStepConfig();

    /**
     * 删除插件配置
     */
    void deleteSecondStepConfig();

    /**
     * 保存平台配置
     * @param jsonObject
     */
    void savePlatformConfig(String platformName, BaseConfig jsonObject);

    /**
     * 查询平台配置
     *
     * @return
     */
    <T extends BaseConfig> T getPlatformConfig(String platformName, Class<T> clazz);

    /**
     * 删除平台配置
     */
    void deletePlatformConfig(String platformName);



}
