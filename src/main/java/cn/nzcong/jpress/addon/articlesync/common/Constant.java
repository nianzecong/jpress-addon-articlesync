package cn.nzcong.jpress.addon.articlesync.common;

import io.jboot.Jboot;

/**
 * 常量
 *
 * @author zecong.nian
 */
public class Constant {

    public static final String DATE_FORMAT_STANDARD = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_MINUTE = "yyyy-MM-dd HH:mm";

    // UTF8
    public static final String CHARSET_UTF8 = "utf-8";

    // 配置的cookie加密key
    public static final String JBOOT_COOKIEENCRYPTKEY = "jboot.web.cookieEncryptKey";

    // 配置的cookie加密密钥
    public static final String CRYPT_KEY = Jboot.configValue(Constant.JBOOT_COOKIEENCRYPTKEY);

    // 账号设置key
    public static final String OPTION_ACCOUNT_PREFIX(String platformName){
        return "articlesync_account_" + platformName;
    }

    // 插件ID
    public static String ADDON_ID;
    // 插件版本
    public static String ADDON_VER;
    // 作者地址
    public static String AUTHOR_WEBSITE;

    // 插件配置
    public static final String OPTION_SSA_CONFIG_KEY = "articlesync_ssa_config";

    // 两步验证cookie的key值
    public static final String SECOND_AUTH_COOKIE_KEY = "j_ssa";

    // 两步验证cookie的有效时间
    public static final int SECOND_AUTH_COOKIE_TIME = 600;


}
