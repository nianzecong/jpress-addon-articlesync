package cn.nzcong.jpress.addon.articlesync.common.Exception;

/**
 * 外部错误
 *
 * @author zecong.nian
 */
public class ExternalServiceException extends BaseException {

    @Override
    ErrorCode getErrorCode() {
        return ErrorCode.EXTERNAL_ERROR;
    }

    public ExternalServiceException(){
        super();
    }

    public ExternalServiceException(Throwable e, String msg, String ... args){
        super(e, msg, args);
    }

    public ExternalServiceException(String msg, String ... args){
        super(msg, args);
    }

    public ExternalServiceException(Throwable e, String msg){
        super(e, msg);
    }

    public ExternalServiceException(String msg){
        super(msg);
    }

}
