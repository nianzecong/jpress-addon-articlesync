package cn.nzcong.jpress.addon.articlesync.common.Exception;

/**
 * 内部错误
 *
 * @author zecong.nian
 */
public class SecondStepAuthException extends BaseException {

    @Override
    ErrorCode getErrorCode() {
        return ErrorCode.AUTH_ERROR;
    }

    public SecondStepAuthException(){
        super();
    }

    public SecondStepAuthException(Throwable e, String msg, String ... args){
        super(e, msg, args);
    }

    public SecondStepAuthException(String msg, String ... args){
        super(msg, args);
    }

    public SecondStepAuthException(Throwable e, String msg){
        super(e, msg);
    }

    public SecondStepAuthException(String msg){
        super(msg);
    }

}
