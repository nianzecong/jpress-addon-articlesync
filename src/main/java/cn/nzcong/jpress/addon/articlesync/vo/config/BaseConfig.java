package cn.nzcong.jpress.addon.articlesync.vo.config;

import cn.nzcong.jpress.addon.articlesync.common.Constant;
import cn.nzcong.jpress.addon.articlesync.common.util.DESUtil;
import com.alibaba.fastjson.JSON;
import lombok.Builder;
import lombok.Data;

/**
 * 基础配置信息
 *
 * @author zecong.nian
 */
@Data
public abstract class BaseConfig {

    /**
     * 配置是否启用，默认不启用
     */
    private boolean enable = false;

    /**
     * 转换为加密json，用于入库
     *
     * @return
     */
    public String toEncryptString(){
        try {
            return DESUtil.encrypt(JSON.toJSONString(this), Constant.CRYPT_KEY);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
