package cn.nzcong.jpress.addon.articlesync.vo.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 简书配置信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JianshuConfig extends BaseConfig{

    private String slug;

    private Long notebook;

    private String cookie;

}
