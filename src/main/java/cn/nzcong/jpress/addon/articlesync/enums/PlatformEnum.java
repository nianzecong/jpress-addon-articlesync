package cn.nzcong.jpress.addon.articlesync.enums;

/**
 * 平台枚举
 */
public enum PlatformEnum {

    JIAN_SHU("jianshu", "简书"),
    TOU_TIAO("toutiao", "头条"),
    CSDN_BLOG("csdn", "CSDN"),
    CNBLOGS("cnblogs", "博客园"),
    ;
    String name;
    String displayName;

    PlatformEnum(String name, String displayName){
        this.name = name;
        this.displayName = displayName;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }
}
