package cn.nzcong.jpress.addon.articlesync.common.Exception;

/**
 * 内部错误
 *
 * @author zecong.nian
 */
public class AuthException extends BaseException {

    @Override
    ErrorCode getErrorCode() {
        return ErrorCode.AUTH_ERROR;
    }

    public AuthException(){
        super();
    }

    public AuthException(Throwable e, String msg, String ... args){
        super(e, msg, args);
    }

    public AuthException(String msg, String ... args){
        super(msg, args);
    }

    public AuthException(Throwable e, String msg){
        super(e, msg);
    }

    public AuthException(String msg){
        super(msg);
    }

}
