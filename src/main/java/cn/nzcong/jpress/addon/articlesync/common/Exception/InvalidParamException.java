package cn.nzcong.jpress.addon.articlesync.common.Exception;

/**
 * 参数错误
 *
 * @author zecong.nian
 */
public class InvalidParamException extends BaseException {

    @Override
    ErrorCode getErrorCode() {
        return ErrorCode.PARAM_ERROR;
    }

    public InvalidParamException(){
        super();
    }

    public InvalidParamException(Throwable e, String msg, String ... args){
        super(e, msg, args);
    }

    public InvalidParamException(String msg, String ... args){
        super(msg, args);
    }

    public InvalidParamException(Throwable e, String msg){
        super(e, msg);
    }

    public InvalidParamException(String msg){
        super(msg);
    }

}
