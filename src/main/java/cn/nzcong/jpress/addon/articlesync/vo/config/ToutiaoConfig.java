package cn.nzcong.jpress.addon.articlesync.vo.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ToutiaoConfig extends BaseConfig{

    private Long mediaId;

    private String cookie;

    private boolean draftOnly = true;

}
