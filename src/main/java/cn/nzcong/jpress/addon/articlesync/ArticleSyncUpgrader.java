package cn.nzcong.jpress.addon.articlesync;

import io.jpress.core.addon.AddonInfo;
import io.jpress.core.addon.AddonUpgrader;


public class ArticleSyncUpgrader implements AddonUpgrader {
    @Override
    public boolean onUpgrade(AddonInfo oldAddon, AddonInfo thisAddon) {
        System.out.println("ArticleSyncUpgrader.onUpgrade()");
        return true;
    }

    @Override
    public void onRollback(AddonInfo oldAddon, AddonInfo thisAddon) {
        System.out.println("ArticleSyncUpgrader.onRollback()");
    }
}
