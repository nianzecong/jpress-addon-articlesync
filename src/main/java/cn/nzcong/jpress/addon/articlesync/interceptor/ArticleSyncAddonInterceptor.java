package cn.nzcong.jpress.addon.articlesync.interceptor;


import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;


public class ArticleSyncAddonInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation inv) {

//        System.out.println("ArticleSyncAddonInterceptor invoke");

        inv.invoke();
    }
}
