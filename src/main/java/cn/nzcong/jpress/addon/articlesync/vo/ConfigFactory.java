package cn.nzcong.jpress.addon.articlesync.vo;

import cn.nzcong.jpress.addon.articlesync.common.Constant;
import cn.nzcong.jpress.addon.articlesync.common.util.DESUtil;
import cn.nzcong.jpress.addon.articlesync.vo.config.BaseConfig;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

public class ConfigFactory {

    /**
     * 从json或加密字符串构建
     *
     * @param json
     * @return
     */
    public static <T extends BaseConfig>T getByString(String json, Class<T> clazz){
        if(StringUtils.isBlank(json)){
            return null;
        }
        if(json.startsWith("{") && json.endsWith("}")){
            return JSONObject.parseObject(json, clazz);
        } else {
            try {
                return JSONObject.parseObject(DESUtil.decrypt(json, Constant.CRYPT_KEY), clazz);
            } catch (Exception e) {
                return null;
            }
        }
    }

}
