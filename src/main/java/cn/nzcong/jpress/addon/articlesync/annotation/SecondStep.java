package cn.nzcong.jpress.addon.articlesync.annotation;


import java.lang.annotation.*;

/**
 * 需要两步验证
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SecondStep {
}
