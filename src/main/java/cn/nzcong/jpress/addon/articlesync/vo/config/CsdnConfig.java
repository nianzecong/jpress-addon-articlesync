package cn.nzcong.jpress.addon.articlesync.vo.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * CSDN博客配置信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CsdnConfig extends BaseConfig{

    private String username;

    private String cookie;

}
